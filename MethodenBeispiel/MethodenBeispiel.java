public class MethodenBeispiel
{
	public static void main(String[] args)
	{
		int zahl1 = 50;
		int zahl2 = 7;

		int erg = max(zahl1, zahl2);

		System.out.println("Ergebnis: " + erg);
	}

	public static int min(int zahl1, int zahl2)
	{
		return (zahl1 < zahl2) ? zahl1 : ((zahl2 < zahl1) ? zahl2 : 0);
	}

	public static int max(int ... vars)
	{
		int max = vars[0];
		for (int i : vars)
			max = (i > max) ? i : max;
		return max;
	}
}
