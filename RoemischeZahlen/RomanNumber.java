public class RomanNumber
{
	public static int convert(String romanStr)
	{
		int value = 0;
		int prevn = 0;
		int samec = 0;
		boolean minus = false;
		for (char c : romanStr.toCharArray()) {
			int prev = prevn;
			int num = toInt(c);
			if (num == -1) {
				System.out.println("Error: '" + c + "' is not a roman number");
				return -1;
			}

			prevn = num;

			if (prev == 0) {
				value = num;
				continue;
			}


			if (isMinus(prev, num)) {
				if (minus || (samec != 0)) {
					System.out.println("Error: invalid roman number format");
					return -1;
				}
				value += num - prev - prev;
				minus = true;
			} else {
				if (prev == num) {
					samec += 1;
				} else {
					samec = 0;
				}
				minus = false;
				value += num;
			}
		}
		return value;
	}

	private static boolean isMinus(int prev, int next)
	{
		if (prev < next)
			return true;
		else
			return false;
	}

	private static int toInt(char c)
	{
		switch(c) {
		case 'I':
		case 'i':
			return 1;
		case 'V':
		case 'v':
			return 5;
		case 'X':
		case 'x':
			return 10;
		case 'L':
		case 'l':
			return 50;
		case 'C':
		case 'c':
			return 100;
		case 'D':
		case 'd':
			return 500;
		case 'M':
		case 'm':
			return 1000;
		default:
			return -1;
		}
	}
}
