import java.util.Scanner;

public class Main
{
	public static void main(String[] args)
	{
		System.out.print("Enter a roman number: ");
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		int num = RomanNumber.convert(input);
		if (num != -1) {
			System.out.println("Number converted is: " + num);
		}
	}
}
